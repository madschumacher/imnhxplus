import numpy as np
from numpy import linalg
from math import *
import scipy.sparse.linalg as spla
import os.path



inpf=open("en.dat", "r")

Nprint=5
Nmesh=50000
Emax=5000. # in cm-1
m=np.array([4.0, 4.0, 4.0])
#UseSimple2ndDer=True
UseSimple2ndDer=False
NDlvl=6
ScatterUniformly=False
UseNPforDiag=False
ReuseIncrements=True
ReuseHmtrx=True

if ReuseIncrements and not os.path.isfile("increments.npy"):
    ReuseIncrements=False
    
if ReuseHmtrx and not os.path.isfile("hamiltonian.npy"):
    ReuseHmtrx=False


############### Read .dat file with energies #######################
xyz=[]
V=[]
for line in inpf:
    words=line.split()
    if len(words)>=4:
        xyz.append(np.array(words[:3],dtype=float))
        V.append(float(words[3]))



inpf.close()
############### Read .dat file with energies (end) #######################


##################### obtain grid points with potential energy less then pre-set criterion ################ 
###################### i.e. (V - Vmin)<=threshold ##########################################
minV=min(V)

the_xyz=[]
the_V=[]
for i,vi in enumerate(V):
    if ((vi - minV)*219474.63)<=Emax:
        the_xyz.append(xyz[i])
        the_V.append(vi)

print str(len(the_V))+" points out of "+str(len(V))+" left"
##################### obtain grid points with potential energy less then pre-set criterion (end) ################ 

dx=np.array([1.0e10, 1.0e10, 1.0e10])
if ReuseIncrements:
    dx=np.load("increments.npy")
else:
################ determine increments ################
    dx=np.array([1.0e10, 1.0e10, 1.0e10])
    
    
    for i in range(0,len(V)-1):
        for j in range(i+1, len(V)):
            for n in range(0,3):
                tdx=abs(xyz[i][n] - xyz[j][n])
                if tdx>1.0e-5 and tdx<dx[n]:
                    dx[n]=tdx
    np.save("increments.npy", dx)
################ determine increments (end) ################

print "Increments: "
print dx


################## Reload pre-computed hamiltonian ####################
if ReuseHmtrx:
    H=np.load("hamiltonian.npy")
else: 
#################### Form DVR hamiltonian matrix #################
    
    H=np.zeros([len(the_V), len(the_V)])
    
    for i in range(0, len(the_V)):
        for j in range(i, len(the_V)):
            # diagonal element of hamiltonian
            if i==j:
                H[i][j]+=(the_V[i]-minV)*219474.63
    
    
            ################### second derivatives are given by formula ##############
            ################## f''(x) = (f(x+h) - 2*f(x) + f(x-h))/(h**2) #####################
            if UseSimple2ndDer:
                if i==j:
                    for n in range(0,3):
                        H[i][j]+=2.*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
                else:
                    # calculate non-diagonal contributions from kinetic energy terms on x,y and z directions
                    for n in range(0,3):                                            
                        if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - dx[n])<1.0e-4:
                            isitgood=True
                            for k in range(1,3):
                                if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                    isitgood=False
                            if isitgood:
                                H[i][j]-=(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
            else:
                # Taken from https://en.wikipedia.org/wiki/Finite_difference_coefficient
                if NDlvl==2:
                    if i==j:
                        for n in range(0,3):
                            H[i][j]-=(-2.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
                    else:
                        for n in range(0,3):
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 1.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(1.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
    
    
                if NDlvl==4:
                    if i==j:
                        for n in range(0,3):
                            H[i][j]-=(-5./2.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
                    else:
                        for n in range(0,3):
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 1.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(4./3.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 2.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(-1./12.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                if NDlvl==6:
                    if i==j:
                        for n in range(0,3):
                            H[i][j]-=(-49./18.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
                    else:
                        for n in range(0,3):
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 1.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(3./2.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 2.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(-3./20.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 3.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(1./90.)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
    
    
                if NDlvl==8:
                    if i==j:
                        for n in range(0,3):
                            H[i][j]-=(-205.0/72.0)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
                    else:
                        for n in range(0,3):
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 1.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(8.0/5.0)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 2.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(-1.0/5.0)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 3.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(8.0/315.0)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                            if abs(abs(the_xyz[i][n] - the_xyz[j][n]) - 4.*dx[n])<1.0e-4:
                                isitgood=True
                                for k in range(1,3):
                                    if not abs(the_xyz[i][n-k] - the_xyz[j][n-k])<1.0e-4:
                                        isitgood=False
                                if isitgood:
                                    H[i][j]-=(-1.0/560.0)*(16.858733678/(m[n]*(dx[n]*0.52917721067)**2))
    
                H[j][i]=H[i][j]
    
    np.save("hamiltonian.npy", H)
#################### Form DVR hamiltonian matrix (end) #################                 


    
print "Hamiltonian is ready"
if UseNPforDiag:
    E,wfn=linalg.eig(H)  ####### solve Schroedinger equation
else:
    #E,wfn=spla.eigsh(H, k=Nprint, which='SA')
    E,wfn=spla.eigsh(H, k=Nprint, which='LA', sigma=10.)

indsort=np.argsort(E)
print "Equation was solved"



for n in range(0,Nprint):
    print "#%3i  %20.15f  %20.15f "  % (n, E[indsort[n]], E[indsort[n]]-E[indsort[0]])
    wfn[:,indsort[n]]/=sqrt(np.sum([a**2 for a in wfn[:,indsort[n]]]))
    outfp=open("wfn_"+str(n)+"_+.dat", "w")
    outfm=open("wfn_"+str(n)+"_-.dat", "w")
    outf=open("wfn_"+str(n)+"_normal.dat", "w")

    outfp.write("# %15.5f \n" % (E[indsort[n]]))
    outfm.write("# %15.5f \n" % (E[indsort[n]]))
    
    for i in range(0, len(the_V)):
        iNdots=int(float(Nmesh)*wfn[i][indsort[n]]**2)
        outf.write(" %15.10f  %15.10f  %15.10f    %15.10f   %15i \n" % (the_xyz[i][0], the_xyz[i][1], the_xyz[i][2], wfn[i][indsort[n]], iNdots))
        for t in range(0, iNdots):
            if ScatterUniformly:
                rndvec=np.array([dx[j]*(np.random.rand()-0.5) for j in range(0,3)])
            else:
                rndvec=np.array([np.random.normal(scale=dx[j]) for j in range(0,3)])          
            tr=(the_xyz[i])+rndvec
            if wfn[i][indsort[n]]>0.0:
                outfp.write(" %15.10f  %15.10f  %15.10f \n" % (tr[0], tr[1], tr[2]))
            else:            
                outfm.write(" %15.10f  %15.10f  %15.10f \n" % (tr[0], tr[1], tr[2]))

    outf.close()
    outfp.close()
    outfm.close()   


exit()









































