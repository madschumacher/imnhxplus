#! /bin/bash


f2c=( $(ls | grep 'mol_[1-4]_1[0-9]\.trj$') )


printf "" > trj_assignment.txt

for f in ${f2c[@]}
do
    echo $f
    name=$(echo $f | sed 's/\.trj//g')

    python analyze_trj_v0.py $f > $name\_ar_pos.dat
    echo $f >> trj_assignment.txt
    python assign_confs_v0.py $name\_ar_pos.dat >> trj_assignment.txt

done