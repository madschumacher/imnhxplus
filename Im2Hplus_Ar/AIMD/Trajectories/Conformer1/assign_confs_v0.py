import numpy as np
from math import *
import sys

fname="conf1.dat"

if len(sys.argv)>1:
    fname=sys.argv[1]

f2r=open(fname, "r")


Nall=0
Nsigma=0
Npi=0
Ncent=0
Nunk=0
Ndecay=0


Angle=45.
Rmax=10.


Nignore=1000

nl=0
for line in f2r:
    words=line.split()
    nl+=1
    if len(words)>5 and nl>Nignore:
        pt=np.array(words, dtype=float)
        Nall+=1
        if pt[1]>=Rmax:
            Ndecay+=1
        else:
            if pt[6]<90. and pt[7]<90. and pt[8]<90.0:
                Ncent+=1
            else:
                i2c=3
                if pt[2]>pt[4]:
                    i2c=5
                if np.abs(pt[i2c])>=(90.-Angle):
                    Npi+=1
                elif np.abs(pt[i2c])<=Angle:
                    Nsigma+=1
                else:
                    Nunk+=1


print(" %i total ponts:\n" % (Nall))
print(" %i (%.3f %%) of central" % ( Ncent, 100.*float(Ncent)/float(Nall)) )
print(" %i (%.3f %%) of sigma" % ( Nsigma, 100.*float(Nsigma)/float(Nall)) )
print(" %i (%.3f %%) of pi" % ( Npi, 100.*float(Npi)/float(Nall)) )
print(" %i (%.3f %%) of decayed" % (Ndecay, 100.*float(Ndecay)/float(Nall)) )
print(" %i (%.3f %%) of unassigned" % (Nunk, 100.*float(Nunk)/float(Nall)) )