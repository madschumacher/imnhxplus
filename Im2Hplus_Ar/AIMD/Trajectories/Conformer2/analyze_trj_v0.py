import numpy as np
from math import *
import re
from scipy.ndimage import gaussian_filter1d as gf1d
import sys


def Atn2Mass(x):
    m=0.
    if re.match('^h$',x, re.IGNORECASE):
        m=1.0
    elif re.match('^c$',x, re.IGNORECASE):
        m=12.0
    elif re.match('^n$',x, re.IGNORECASE):
        m=14.0
    elif re.match('^ar$',x, re.IGNORECASE):
        m=40.0 #39.9623831225
    return m

def CalcAndPrintProperties(m, xyz, outf):
    nat=len(m)
    m=np.array(m)
    xyz=np.array(xyz)
    #print(m,xyz)
    rings=np.array([[ 1, 2, 5, 6, 7],
                    [11,12,13,14,15]])

    Rcc = np.zeros((len(rings),3))
    Nc  = np.zeros((len(rings),3))


    nAr=20

    nAr-=1


    Rcm=np.zeros(3)
    M=0.0
    for i in range(0,nat):
        if i!=nAr:
            Rcm+=m[i]*xyz[i]
            M+=m[i]

    Rcm/=M
    
    print(" %10.4f " %  np.sqrt(np.dot( xyz[nAr] - Rcm, xyz[nAr] - Rcm)) ),
    #madprinter.write(" %7.4f " % np.dot( xyz[nAr] - Rcm, xyz[nAr] - Rcm) )

    RAr=[]
    for n in range(0,len(rings)):
        rings[n] -= 1
        #print(rings[n])

        Mrc=np.array([m[i] for i in rings[n] ])
        XYZrc=np.array([(xyz[i]) for i in rings[n]])
        #print(Mrc,XYZrc)
        for i in rings[n]:
            Rcc[n]+=m[i]*xyz[i]

        cMass=np.sum(Mrc)
        Rcc[n]/=cMass
        #print(Rcc[n],XYZrc)

        #outf=open(dump_file, "a")
        outf.write("%i\n# ring #%i\n" % (len(Mrc)+1+1, n))      
        outf.write(" X %7.4f %7.4f %7.4f\n" % tuple(np.zeros(3)))

        for i in range(0,len(Mrc)):
            XYZrc[i]-=Rcc[n]
            outf.write(" C %7.4f %7.4f %7.4f\n" % tuple(XYZrc[i]))


        A=np.array([[np.sum([Mrc*XYZrc[:,a]*XYZrc[:,b]])/cMass  for b in range(0,3)] for a in range(0,3)])
       
        w,v = np.linalg.eig(A)

        imin=np.argmin(np.abs(w))
        
        nvec=v[:,imin]
        outf.write(" X %7.4f %7.4f %7.4f\n" % tuple(nvec))

        rAr=xyz[nAr]-Rcc[n]
        
        angle=(180./np.pi)*np.arccos(np.dot(nvec,rAr)/np.sqrt(np.dot(nvec,nvec)*np.dot(rAr,rAr)))
        angle-=90.0
        #angle=(180./np.pi)*np.arcsin(np.dot(nvec,rAr)/np.sqrt(np.dot(nvec,nvec)*np.dot(rAr,rAr)))
        dist=np.sqrt(np.dot(rAr,rAr))
        RAr.append(dist)
        print(" %10.4f %10.4f " % (dist,angle)), 
        #outf.close()        


    rCC = np.sqrt(np.dot( (Rcc[0]-Rcc[1]) , (Rcc[0]-Rcc[1]) ))
    cosA = (RAr[0]**2 + RAr[1]**2 - rCC**2)/(2.0*RAr[0]*RAr[1])
    cosB = (RAr[0]**2 + rCC**2 - RAr[1]**2)/(2.0*RAr[0]*rCC)
    cosC = (RAr[1]**2 + rCC**2 - RAr[0]**2)/(2.0*RAr[1]*rCC)

    print(" %15.10f " %  ((180./np.pi)*np.arccos(cosA))),
    print(" %15.10f " %  ((180./np.pi)*np.arccos(cosB))),
    print(" %15.10f " %  ((180./np.pi)*np.arccos(cosC)))
    return Rcc
    
    
    



def ReadTrj(fname):
    inpf=open(fname, "r")


    lc=0
    nat=0

    #Data=[]

    data2save=open(fname+"store_rings.trj", "w")
    trj=open(fname+"_mod.trj", "w")

    count=0
    for line in inpf:
        if lc==0:
            nat=int(line.split()[0])
            #print("Number of atoms = "+str(nat))
            mass=[]

        if lc % (nat+2) == 0: 
            if len(line.split())>0 and nat != int(line.split()[0]):
                print("Number of atoms is inconsistent at line "+str(lc))
                exit()
            if lc>0:
                if len(M)!=nat:
                    print("smth is wrong with reading")
                    exit()
                print(" %10i " % count),
                count+=1
                rcc=CalcAndPrintProperties(M, XYZ, data2save)
                trj.write("%i\n\n" % (nat+len(rcc)))
                for i in range(0,nat):
                    trj.write(" %s  %15.4f %15.4f %15.4f\n" % (Atn[i], XYZ[i][0], XYZ[i][1], XYZ[i][2]))
                for tr in rcc:
                    trj.write(" X   %15.4f %15.4f %15.4f\n" % tuple(tr))
            Atn = []
            M   = []
            XYZ = []

        if lc % (nat+2) > 1:
            Atn.append( line.split()[0])
            XYZ.append( list(np.array(line.split()[1:4] ,dtype=float)) )
            M.append(Atn2Mass(line.split()[0]))

        lc+=1    
    if len(M)==nat:  
        print(" %10i " % count), 
        CalcAndPrintProperties(M, XYZ, data2save)
    data2save.close()


fnames=["test.xyz"]

if len(sys.argv)>1:
    fnames=sys.argv[1:]

for fn in fnames:
    ReadTrj(fn)



