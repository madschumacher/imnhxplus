import numpy as np
from math import *
import re
from scipy.ndimage import gaussian_filter1d as gf1d
import sys

def Atn2Mass(x):
    m=0.
    if re.match('^h$',x, re.IGNORECASE):
        m=1.0
    elif re.match('^c$',x, re.IGNORECASE):
        m=12.0
    elif re.match('^n$',x, re.IGNORECASE):
        m=14.0
    elif re.match('^ar$',x, re.IGNORECASE):
        m=40.0 #39.9623831225
    return m


def ReadTrj(fname):
    inpf=open(fname, "r")


    lc=0
    nat=0

    Data=[]
    for line in inpf:
        if lc==0:
            nat=int(line.split()[0])
            print("Number of atoms = "+str(nat))
            mass=[]

        if lc % (nat+2) == 0: 
            if len(line.split())>0 and nat != int(line.split()[0]):
                print("Number of atoms is inconsistent at line "+str(lc))
                exit()
            if lc>0:
                if len(NewVVec)!=3*nat:
                    print("smth is wrong with reading")
                    exit()
                Data.append(NewVVec)

            NewVVec=[]

        if lc % (nat+2) > 1:
            NewVVec+=list(np.array(line.split()[1:4] ,dtype=float))

            if lc<=(nat+2):
                tm=Atn2Mass(line.split()[0])
                mass+=[tm, tm, tm]

        lc+=1    
    if len(NewVVec)==3*nat:    
        Data.append(NewVVec)

    Data=np.array(Data)
    return Data, np.array(mass)

#fname="vel.trj"

Hz2wvn=3.335653E-11
dt=1.0
Nignore=1000


def CalcSpectrum(fname, dt=dt, Nignore=Nignore, DoCorrection=True, vmin=100.):
    VVec, Mass = ReadTrj(fname)

    ACF=np.array([ np.correlate(VVec[Nignore:,i],VVec[Nignore:,i], "same") for i in range(0,len(VVec[0])) ])
    FFT=np.array([np.absolute(np.fft.rfft(acf) )  for acf in  ACF ])

    Npts=len(ACF[0])

    dnu=1.0/(Npts*dt*1.0e-15)

    print(" dnu = %15.10f cm-1 for %s due to %i pts" % (dnu*Hz2wvn, fname, Npts))

    Sp=np.matmul(FFT.T, Mass)
    nu=np.array([0.0+i*dnu for i in range(0,len(Sp))])

    if DoCorrection:
        nu=np.sqrt(2.*(1. - np.cos(2.*pi*dt*1.0e-15*nu)))/(2.*pi*dt*1.0e-15)

    nu*=Hz2wvn

    nu2Print=nu[nu<4200.0]
    Sp2Print=Sp[nu<4200.0]

    icut=int(vmin/(dnu*Hz2wvn))

    return nu2Print[icut:],Sp2Print[icut:]

if len(sys.argv)>1:
    fnames=sys.argv[1:]

    v=[]
    I=[]
    for fname in fnames:
        tv,tI = CalcSpectrum(fname, dt=dt, Nignore=Nignore, DoCorrection=True)
        v.append(tv)
        I.append(tI)

    for i in range(1,len(v)):
        if len(v[0])!=len(v[i]):
            print("trj sizes for %s and %s are different" % (fnames[0], fnames[i]) )
            exit()


    #avgI=np.average(I, axis=0)
    #errI=np.std(I, axis=0)

    Niter=0
    Nsp=len(I)
    sc = [1.0 for i in range(0,Nsp)]



    vmin=3400.
    vmax=3700.
   
#    dv=min(v[0][1:] - v[0][:-1])

    imin=np.max(np.argwhere(v[0]<=vmin))
    imax=np.min(np.argwhere(v[0]>=vmax))
    print(imin,imax,v[0][imin],v[0][imax])

    w = np.full(len(I[0][imin:imax]),1.0)
    for n in range(0,Niter):
        avgI=np.average([I[i][imin:imax]*sc[i] for i in range(0,Nsp)], axis=0)
        sc = [np.sum(w*avgI*I[i][imin:imax])/np.sum(w*I[i][imin:imax]*I[i][imin:imax]) for i in range(0,Nsp)]
        print(n,sc)
        w = 1.0/np.std([I[i][imin:imax]*sc[i] for i in range(0,Nsp)], axis=0)**2
        #errI=np.std([I[i]*sc[i] for i in range(0,Nsp)], axis=0)

        
    avgI=np.average([I[i]*sc[i] for i in range(0,Nsp)], axis=0)
    errI=np.std([I[i]*sc[i] for i in range(0,Nsp)], axis=0)
    #newI=[(tI - avgI)/errI for tI in I]

    np.savetxt("averaged_spectra.res", np.stack([v[0], avgI, errI], axis=-1) )
    np.savetxt("all_spectra.res", np.stack([v[0]] + [I[i]*sc[i] for i in range(0,Nsp)] ,axis=-1) )

else:
    print("NO INPUT FILE")
    exit()


exit()





VVec, Mass = ReadTrj(fname)

print(Mass)


Nignore=1000

ACF=np.array([ np.correlate(VVec[Nignore:,i],VVec[Nignore:,i], "same") for i in range(0,len(VVec[0])) ])



dt=1.0


FFT=np.array([np.absolute(np.fft.rfft(acf) )  for acf in  ACF ])
Npts=len(ACF[0])

Hz2wvn=3.335653E-11

dnu=1.0/(Npts*dt*1.0e-15)
print(" dnu = %15.10f cm-1 " % (dnu*Hz2wvn))

Sp=np.matmul(FFT.T, Mass)

#nu=np.arange(0.0, dnu*len(Sp), dnu)
nu=np.array([0.0+i*dnu for i in range(0,len(Sp))])

DoCorrection=True
#DoCorrection=False

if DoCorrection:
    nu=np.sqrt(2.*(1. - np.cos(2.*pi*dt*1.0e-15*nu)))/(2.*pi*dt*1.0e-15)

#ScaleFactor=1.0
ScaleFactor=0.97101227  # Fitted to C-H
#ScaleFactor=0.9671  #  Fundamental @ B3LYP/def2-SVP
#ScaleFactor=0.9912  #  ZVPE @ B3LYP/def2-SVP


nu*=Hz2wvn*ScaleFactor


nu2Print=nu[nu<5000.0]
Sp2Print=Sp[nu<5000.0]

wfilter=7.0 # cm-1

wsigma=wfilter/(dnu*Hz2wvn)

print(wsigma)
cleanSp=gf1d(Sp2Print, sigma=wsigma)


Res2Print=np.stack( [nu2Print,Sp2Print], axis=-1)
np.savetxt(fname+"_raw_sp.dat", Res2Print)

Res2Print=np.stack( [nu2Print,cleanSp], axis=-1)
print(Res2Print)
np.savetxt(fname+"_clean_sp.dat", Res2Print)







