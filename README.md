# ImnHXplus

The scripts and data for the (ImH)_n^+...X (n=1,2,3; X=He,Ar) results

# Contents
This repository contains the in-house codes and data examples for reproducing the results on the protonated imidazole clusters.

## ImHplus_He
This folder contains the 3D Schroedinger equation parameters, script for solving this equation for movement of helium around protonated imidazole monomer, and the scripts used for plotting the results.

## Im2Hplus_Ar
This folder contains the ab initio molecular dynamics (AIMD) analysis scripts, that were used to determine the ratio of the sigma- and pi- conformers in the MD trajectories. The power spectra computation scripts are also provided.

